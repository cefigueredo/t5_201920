package model.logic;

import java.util.Iterator;

public class UBERTrip implements Comparable<UBERTrip> {
	private double sourceid;
	private double dstid;
	private double meanTravelTime;
	private double standardDeviationTravelTime;
	private double geometricMeanTravelTime;
	private double geometricStandardDeviationTravelTime;
	
	public UBERTrip(double pSourceid, double pDstid, double pMeanTravelTime, double pStandardDeviationTravelTime, double pGeometricMeanTravelTime, double pGeometricStandardDeviationTravelTime){
		sourceid = pSourceid;
		dstid = pDstid;
		meanTravelTime = pMeanTravelTime;
		standardDeviationTravelTime=pStandardDeviationTravelTime;
		geometricMeanTravelTime = pGeometricMeanTravelTime;
		geometricStandardDeviationTravelTime = pGeometricStandardDeviationTravelTime;
	}
	
	public double getSourceid() {
		return sourceid;
	}

	public double getDstid() {
		return dstid;
	}

	public double getMeanTravelTime() {
		return meanTravelTime;
	}


	public double getStandardDeviationTravelTime() {
		return standardDeviationTravelTime;
	}

	public double getGeometricMeanTravelTime() {
		return geometricMeanTravelTime;
	}



	public double getGeometricStandardDeviationTravelTime() {
		return geometricStandardDeviationTravelTime;
	}

	
	
	
	public int compareTo(UBERTrip o) {
		// TODO Auto-generated method stub
		int rta=0;
		if(meanTravelTime>o.getMeanTravelTime())
		{
			rta=1;
		}
		else if(meanTravelTime==o.getMeanTravelTime())
		{
			if(standardDeviationTravelTime>o.getStandardDeviationTravelTime())
			{
				rta=1;
			}
			else if(standardDeviationTravelTime<o.getStandardDeviationTravelTime())
			{
				rta=-1;
			}
			else
			{
				rta=0;
			}
		}
		else
		{
			rta=-1;
		}
		return rta;
	}

	
	

	


}
