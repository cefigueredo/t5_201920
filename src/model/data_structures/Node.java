package model.data_structures;

public class Node<T> {
	private ArregloDinamico<T> elements;
	private Node next;
	public Node(ArregloDinamico<T> element)
	{
		this.elements=element;
		next=null;
	}
	public void setNext(Node nextN)
	{
		next=nextN;
	}
	public void addElement(T newEl)
	{
		elements.agregar(newEl);
	}
	public Node giveNext()
	{
		return next;
	}
	public ArregloDinamico<T> giveValues()
	{
		return elements;
	}
}
