package model.data_structures;

public class LinkedList<T> implements ILinkedList<T> {

	private int tamanio;
	private Node<T> first;
	private Node<T> last;
	public LinkedList() {
		// TODO Auto-generated constructor stub
		tamanio=0;
		first=null;
		last=null;
	}
	@Override
	public int darTamanio() {
		// TODO Auto-generated method stub
		return tamanio;
	}

	@Override
	public Node<T> darPrimero() {
		// TODO Auto-generated method stub
		return first;
	}

	@Override
	public void addFirst(Node<T> pNode) {
		// TODO Auto-generated method stub
		if(tamanio==0)last=pNode;
		if(tamanio==1)last=first;
		Node <T> aux= first;
		first=pNode;
		first.setNext(aux);
		tamanio++;
	}

	@Override
	public void add(Node<T> pNode) {
		// TODO Auto-generated method stub
		if(last==null) 
		{
			first=pNode;
			last=first;
			return;
		}
		Node<T> aux=last;
		aux.setNext(pNode);
		last=pNode;
		
	}
	@Override
	public Node<T> darUltimo() {
		// TODO Auto-generated method stub
		return last;
	}
	
}
