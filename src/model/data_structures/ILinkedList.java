package model.data_structures;
import model.data_structures.Node;

import model.logic.UBERTrip;

public interface ILinkedList<T> {
	
	int darTamanio();
	Node<T> darPrimero();
	Node<T> darUltimo();
	void addFirst(Node<T> pNode);
	void add(Node<T> pNode);
}
